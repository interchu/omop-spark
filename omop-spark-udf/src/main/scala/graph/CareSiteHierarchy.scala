/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.spark.omop.graph

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
  * This generate a care_site_hierarchy table
  * with the above and below array columns containing
  * for a given care_site_id all its parents or childrens
  *
  * This is at least usefull for the FHIR organization resource
  * which need to know by advance all the ancestor/children for
  * given Organization
  * see: https://www.hl7.org/fhir/search.html#recursive
  */

object CareSiteHierarchy extends App {

  val spark = SparkSession
    .builder()
    .appName("OMOP person secu calc")
    .enableHiveSupport()
    .getOrCreate()

  val edgeDF = spark.read
    .table("edsomop.fact_relationship")
    .filter(
      col("domain_concept_id_1") === lit(57)
        && col("domain_concept_id_2") === lit(57)
        && col("relationship_concept_id") === lit(46233689)
    )
    .dropDuplicates("fact_id_1", "fact_id_2")
    .selectExpr("fact_id_1 as src", "fact_id_2 as dest")

  val empVertexDF = spark.read
    .table("edsomop.care_site")
    .dropDuplicates("care_site_id")
    .selectExpr("care_site_id", "care_site_name")

  val hg = new HeritageGraph

  // call the function
  val hierarchyDF = {

    hg.calcTopLevelHierarcy(empVertexDF, edgeDF, 100)

  }

  {

    spark.read
      .table("edsomop.orbis_visit_detail")
      .as("v")
      .repartition(1000)
      .join(
        hierarchyDF.alias("g"),
        col("v.care_site_id") === col("g.id"),
        "left"
      )
      .groupBy("person_id")
      .agg(
        array_sort(array_distinct(flatten(collect_list(col("g.path")))))
          .as("agg_care_site_id")
      )
      .write
      .format("orc")
      .mode(org.apache.spark.sql.SaveMode.Overwrite)
      .saveAsTable("edsomop.person_secu")

  }
}
