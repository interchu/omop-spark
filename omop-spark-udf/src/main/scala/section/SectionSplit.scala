/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.section

import java.util.regex.Pattern

import org.apache.spark.sql.functions.{col, explode, udf}
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.collection.mutable.ListBuffer

object SectionSplit extends App {

  val indb = args(0)
  val input_table = args(1) // 'note_id, 'note_text, 'note_class_source_value
  val outdb = args(2)
  val output_table = args(3) // note_id | sentence | words | postag

  val spark = SparkSession
    .builder()
    .appName("OMOP Section Extract")
    .enableHiveSupport()
    .getOrCreate()

  import spark.implicits._

  // get the section file
  // label_section is the exact content of the section
  // cd_section is the concept_code of the section
  // cd_doc is the concept_code of the document in which the section might exist
  val note = spark.read
    .table("%s.%s".format(indb, input_table))
    .select('note_id, 'note_text, 'note_class_source_value)

  val refSection = spark.read
    .table("share.ref_doc_section")
    .select('cd_section, 'cd_doc, 'label_section, 'concept_id, 'concept_name)

  spark.sql("drop table if exists %s.%s".format(outdb, output_table))
  explodeSection(spark, refSection, note, -1)
    .select('note_id, 'offset_begin, 'offset_end, 'note_class_source_value,
      'note_text, 'note_class_source_concept_id)
    .write
    .format("orc")
    .mode(org.apache.spark.sql.SaveMode.Overwrite)
    .saveAsTable("%s.%s".format(outdb, output_table))

  def explodeSection(
      spark: SparkSession,
      refSection: DataFrame,
      note: DataFrame,
      headerConceptId: Int,
      numPartitions: Int = 1000
  ): DataFrame = {

    val bc = spark.sparkContext.broadcast(refSection.collect())

    val sectionUdf = udf((_text: String, code: String) => {
      val text =
        if (_text == null) ""
        else _text

      val f = new ListBuffer[Tuple6[Int, Int, String, String, Int, String]]()
      bc.value.foreach({
        row =>
          {
            if (
              (code == null || row.getString(1) == code) && row
                .getString(2) != null
            ) {
              val reg =
                ("\n[\\s]{0,6}(" + Pattern.quote(row.getString(2).trim) + ")").r
              reg
                .findFirstMatchIn(text)
                .foreach(b =>
                  f += (
                    (
                      b.start,
                      b.end,
                      "",
                      row.getString(0),
                      row.getInt(3),
                      row.getString(4)
                    )
                  )
                )
            }
          }
      })

      // get longest match for each index
      var x =
        new ListBuffer[Tuple6[Int, Int, String, String, Int, String]]().toList
      if (f.size == 0) {
        x = List((0, text.length, "ENTETE", text, headerConceptId, "ENTETE"))
      } else {
        val c = f
          .groupBy(f => f._1)
          .map(g => g._2.sortWith(_._2 > _._2)(0))
          .toList
          .sortWith(_._1 < _._1)
        if (c.size == 1) {
          val d = c
            .sliding(1)
            .map(m => (m(0)._1, text.length, m(0)._4, m(0)._5, m(0)._6))
            .toList :+
            (0, c.head._1, "ENTETE", headerConceptId, "ENTETE")
          x = d.map(m =>
            (m._1, m._2, m._3, text.substring(m._1, m._2), m._4, m._5)
          )
        } else {
          val d = c
            .sliding(2)
            .map(m => (m(0)._1, m(1)._1, m(0)._4, m(0)._5, m(0)._6))
            .toList :+
            (c.last._1, text.length, c.last._4, c.last._5, c.last._6) :+
            (0, c.head._1, "ENTETE", headerConceptId, "ENTETE")
          x = d.map(m =>
            (m._1, m._2, m._3, text.substring(m._1, m._2), m._4, m._5)
          )
        }
      }
      x
    })

    // save the resulting section table
    {

      note
        .repartition(numPartitions)
        .select(
          col("note_id"),
          explode(sectionUdf(col("note_text"), col("note_class_source_value")))
            .as("e")
        )
        .select("note_id", "e.*")
        .withColumnRenamed("_1", "offset_begin")
        .withColumnRenamed("_2", "offset_end")
        .withColumnRenamed("_3", "note_class_source_value")
        .withColumnRenamed("_4", "note_text")
        .withColumnRenamed("_5", "note_class_source_concept_id")
        .withColumnRenamed("_6", "note_title")

    }

  }
}
