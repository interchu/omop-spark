/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.omop.deid

import org.apache.spark.sql.QueryTest

class AppTest extends QueryTest with SparkSessionTestWrapper {

  test("test fhir patient") {

    val df = spark.sql("""
      select 1 as person_id, 4335813 as observation_concept_id, 'boby' as value_as_string 
        union all 
      select 1 as person_id, 4086449 as observation_concept_id, 'bob' as value_as_string 
        union all 
    	select 2 as person_id, 4314148 as observation_concept_id, 'jim' as value_as_string 
    	  union all 
    	select 2 as person_id, 4335813 as observation_concept_id, 'morrisson' as value_as_string
    	  union all 
    	select 2 as person_id, 4083592 as observation_concept_id, 'harrisson' as value_as_string  
      """)

  }

}

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession
      .builder()
      .master("local")
      .appName("spark session")
      .config("spark.sql.shuffle.partitions", "1")
      .getOrCreate()
  }

}
