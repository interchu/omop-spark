OMOP-TERMINOLOGY-LOADER
=======================

This loads concepts into susana.

Usage bash
+++++++++++
.. code-block:: bash
	
        $(SPARK_HOME)/bin/spark-submit \
	--driver-class-path /opt/lib/postgresql-42.2.5.jar  \
	--jars "/opt/lib/scala-logging.jar,/opt/lib/spark-postgres-2.7.0-SNAPSHOT-shaded.jar,/opt/spark/spark-2.4.0-bin-hadoop2.7"  \
	--deploy-mode client \
	--driver-memory=10G  \
	--executor-memory=4G  \
	--class Run /opt/lib/omop-postgres-sync-0.0.1-SNAPSHOT-shaded.jar  "local[5]" "/tmp/spark-postgres-${USER}" "/home/mapper/app/conceptual-mapping/terminologies/" "interchuprojet.csv"
