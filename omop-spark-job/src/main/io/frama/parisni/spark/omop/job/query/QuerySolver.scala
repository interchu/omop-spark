package io.frama.parisni.spark.omop.job.query

import io.frama.parisni.spark.query.Query
import org.apache.spark.sql.{DataFrame, SparkSession}

object QuerySolver {

  def fhirSolrGet(res: BasicResource)(implicit spark: SparkSession,
                                      solrConf: Map[String, String]): Query = {

    import com.lucidworks.spark.util.SolrDataFrameImplicits._
    var df: DataFrame = spark.read
      .solr(res.resourceType,
            solrConf ++ Map("collection" -> res.resourceType,
                            "fields" -> "patient",
                            "filters" -> res.filter))
    df =
      if (res.numOccurrences.isDefined)
        df.groupBy("patient")
          .count
          .filter(s"count >= ${res.numOccurrences.get}")
          .drop("count")
      else df

    Query(df, "whuut")
  }

}
