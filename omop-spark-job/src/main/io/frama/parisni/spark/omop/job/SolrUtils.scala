/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job

import java.sql.{Connection, ResultSet, Statement}

import com.lucidworks.spark.util.SolrSupport
import com.typesafe.scalalogging.LazyLogging
import io.frama.parisni.spark.postgres.PGTool
import org.apache.solr.client.solrj.impl.CloudSolrClient
import org.apache.solr.common.SolrInputDocument
import org.apache.spark.sql.SparkSession

import scala.collection.immutable.HashMap

/**
  * For each solr collection having _list:
  * 1. Get the new cohort to refresh -> newListIds
  * 2. Get the collectionId from the unique the patients in the new cohort
  * 3. Batch update the collection
  */
case class CollectionUpdate(
    collection: String,
    table: String,
    column: String,
    filter: Option[String]
)

object SolrUtils
    extends App
    with SolrUtils
    with PostgresUtils
    with LazyLogging {

  val (pgUrl, solrUrl) = args match {
    case Array(_, _) => (args(0), args(1))
    case _           => throw new Exception("provide both postgres url and solr url")
  }

  val options = Map(
    "zkhost" -> f"$solrUrl",
    "commit_within" -> "10000",
    "batch_size" -> "5000",
    "timezone_id" -> "Europe/Paris"
  )

  val spark = SparkSession
    .builder()
    .appName("Fhir Atomic Update")
    .getOrCreate()

  val collections = List(
    CollectionUpdate("patientAphp", "person", "person_id", None),
    CollectionUpdate(
      "encounterAphp",
      "visit_occurrence",
      "visit_occurrence_id",
      None
    ),
    CollectionUpdate("encounterAphp", "visit_detail", "visit_detail_id", None)
  )

  val clientPostgres = PGTool(spark, pgUrl, "/tmp").getConnection()
  val clientSolr: CloudSolrClient = SolrSupport.getCachedCloudClient(solrUrl)
  clientSolr.setParallelUpdates(true)

  val newCohortIds = getNewCohortDefinitionId(clientPostgres)

  try {
    collections.foreach { collection =>
      clientSolr.setDefaultCollection(collection.collection)

      def updateSolr(ids: List[Long]) =
        updateAtomicBatch(
          clientSolr,
          ids.map(_.toString),
          "_list",
          newCohortIds
        )

      process(
        clientPostgres,
        getCollectionId(collection, newCohortIds),
        50000,
        updateSolr
      )
    }
  } catch {
    case e: Exception => ??? //TODO: implement set job error
  } finally {
    ??? //TODO: implement set job end
    clientPostgres.close()
    clientSolr.close()
  }

}

trait SolrUtils {

  def updateAtomic(id: String, field: String, value: List[Long]) = {
    val sdoc = new SolrInputDocument();
    sdoc.addField("id", id);
    import collection.JavaConverters._
    val m = HashMap("add" -> value.asJavaCollection).asJava
    sdoc.addField(field, m); // add the map as the field value
    sdoc
  }

  def updateAtomicBatch(
      cloudSolrClient: CloudSolrClient,
      ids: List[String],
      field: String,
      value: List[Long]
  ): Unit = {
    ids.foreach(id => cloudSolrClient.add(updateAtomic(id, field, value)))
    cloudSolrClient.commit()
  }

}

trait PostgresUtils {

  def getBatch(rs: ResultSet) = {
    val fetchSize = rs.getStatement.getFetchSize

    new Iterator[List[Long]] {
      def hasNext = rs.next()

      def next() = {
        var count = fetchSize
        val result = scala.collection.mutable.ListBuffer.empty[Long]
        result += rs.getLong(1)
        while (count > 0 && hasNext) {
          count = count - 1
          result += rs.getLong(1)
        }
        result.toList
      }
    }.toStream
  }

  def getResultSet(
      conn: Connection,
      query: String,
      fetchSize: Int
  ): ResultSet = {
    conn.setAutoCommit(false);
    val st: Statement = conn.createStatement();
    st.setFetchSize(fetchSize);
    val rs: ResultSet = st.executeQuery(query);
    rs
  }

  def process(
      conn: Connection,
      query: String,
      fetchSize: Int,
      f: List[Long] => Unit
  ): Unit = {
    val rs = getResultSet(conn, query, fetchSize)
    getBatch(rs).foreach(x => f(x))
    rs.close();
  }

  def getNewCohortDefinitionId(conn: Connection): List[Long] = {
    val query =
      """
        select entity_id
        from job
        join job_definition using (job_definition_id)
        where status_id = 0
        """
    val st: Statement = conn.createStatement();
    val rs: ResultSet = st.executeQuery(query);

    val result: List[Long] = new Iterator[Long] {
      def hasNext = rs.next()

      def next() = rs.getLong(1)
    }.toList

    result
  }

  def getCollectionId(
      table: CollectionUpdate,
      cohortDefinitionIds: List[Long]
  ): String = {
    val query =
      s"""
          select "${table.column}"
          from "${table.table}"
          where ${table.filter.getOrElse("true")}
          where person_id in (
              select subject_id
              from cohort
              where cohort_definition in ${cohortDefinitionIds.mkString(
        "(",
        ",",
        ")"
      )}
          )
        """
    query
  }

}
