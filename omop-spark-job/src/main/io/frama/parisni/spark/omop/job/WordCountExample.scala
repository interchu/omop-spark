/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job

import com.typesafe.config.Config
import org.apache.spark.SparkContext
import org.scalactic._
import spark.jobserver.api.{
  JobEnvironment,
  SingleProblem,
  ValidationProblem,
  SparkJob
}

import scala.util.Try

object WordCountExample extends SparkJob {
  type JobData = Seq[String]
  type JobOutput = collection.Map[String, Long]

  override def runJob(
      sc: SparkContext,
      runtime: JobEnvironment,
      data: JobData
  ): JobOutput =
    sc.parallelize(data).countByValue

  override def validate(
      sc: SparkContext,
      runtime: JobEnvironment,
      config: Config
  ): JobData Or Every[ValidationProblem] = {
    Try(config.getString("input.string").split(" ").toSeq)
      .map(words => Good(words))
      .getOrElse(Bad(One(SingleProblem("No input.string param"))))
  }
}
