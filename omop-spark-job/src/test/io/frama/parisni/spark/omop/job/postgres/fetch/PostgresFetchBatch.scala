/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.job.postgres.fetch

import java.sql.{Connection, DriverManager, ResultSet, Statement, Timestamp}

import io.frama.parisni.spark.omop.job.{
  PostgresUtils,
  SolrConfTest,
  SparkSessionTestWrapper
}
import org.apache.spark.sql.{DataFrame, SaveMode}
import org.junit.Test

class PostgresFetchBatch extends SparkSessionTestWrapper with PostgresUtils { //FunSuite with io.frama.parisni.spark.sync.copy.SparkSessionTestWrapper

  @Test
  def testPg2Pg(): Unit = {

    import spark.implicits._
    println("io.frama.parisni.spark.sync.Sync Postgres To Postgres")

    // Create table "source"
    val s_inputDF: DataFrame = ((
      1L,
      "id1s",
      "PG details of 1st row source",
      Timestamp.valueOf("2016-02-01 23:00:01"),
      Timestamp.valueOf("2016-06-16 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00")
    ) ::
      (
      2L,
      "id2s",
      "PG details of 2nd row source",
      Timestamp.valueOf("2017-06-05 23:00:01"),
      Timestamp.valueOf("2016-06-16 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00")
    ) ::
      (
      3L,
      "id3s",
      "PG details of 3rd row source",
      Timestamp.valueOf("2017-08-07 23:00:01"),
      Timestamp.valueOf("2016-06-16 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00")
    ) ::
      (
      4L,
      "id4s",
      "PG details of 4th row source",
      Timestamp.valueOf("2018-10-16 23:00:01"),
      Timestamp.valueOf("2016-06-16 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00")
    ) ::
      (
      5L,
      "id5",
      "PG details of 5th row source",
      Timestamp.valueOf("2019-12-27 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00")
    ) ::
      (
      6L,
      "id6",
      "PG details of 6th row source",
      Timestamp.valueOf("2020-01-14 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00"),
      Timestamp.valueOf("2016-06-16 00:00:00")
    ) ::
      Nil).toDF(
      "id",
      "pk2",
      "details",
      "date_update",
      "date_update2",
      "date_update3"
    )

    /* reference https://jdbc.postgresql.org/documentation/83/query.html#query-with-cursor */
    s_inputDF.write
      .format("postgres")
      .mode(SaveMode.Overwrite)
      .option("url", getPgUrl)
      .option("type", "full")
      .option("table", "mytable")
      .option("partitions", 4)
      .option("kill-locks", true)
      .save

    val conn = getConn()
    val query = "SELECT * FROM mytable"
    process(conn, query, fetchSize = 3, println)

  }
}
