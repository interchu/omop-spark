package io.frama.parisni.spark.omop.job.query

import io.frama.parisni.spark.omop.job.SparkSessionTestWrapper
import io.frama.parisni.spark.query.{DataFrameEventQuery, Query}

case class BasicDataset(patient: Int, cd: String)
trait QueryDataset extends SparkSessionTestWrapper {

  val df1 =
    DataFrameEventQuery(spark.createDataFrame(BasicDataset(1, "a") :: Nil),
                        "person",
                        "cd")

  val df2 = DataFrameEventQuery(
    spark.createDataFrame(BasicDataset(1, "a") :: BasicDataset(2, "b") :: Nil),
    "df2",
    "cd")

  val df3 =
    DataFrameEventQuery(spark
                          .createDataFrame(BasicDataset(1, "a") :: BasicDataset(
                            2,
                            "b") :: BasicDataset(3, "c") :: Nil),
                        "df3",
                        "cd")

  val df4 =
    DataFrameEventQuery(spark.createDataFrame(
                          BasicDataset(1, "a") :: BasicDataset(2, "a") :: BasicDataset(
                            3,
                            "a") :: BasicDataset(4, "a") :: Nil),
                        "person",
                        "cd")
  implicit def getDf(ent: BasicResource): Query = ent.filter match {
    case "df1" => df1
    case "df2" => df2
    case "df3" => df3
    case "df4" => df4
  }
  /*
   this one says:
   take all patients having 2 among 3
   from tree group of:
   1. patient AND Condition
   2. Observation OR Encounter
   3. Patient MINUS Patient
   */

  val exampleJson =
    """
      |{
      |  "_type": "NamongM",
      |  "count": 2,
      |  "child": [
      |    {
      |      "_type": "InnerJoin",
      |      "child": [
      |        {
      |          "_type": "resource",
      |          "resourceType": "Patient",
      |          "filterSolr": "df1"
      |        },
      |        {
      |          "_type": "resource",
      |          "resourceType": "Condition",
      |          "filterSolr": "df2"
      |        }
      |      ]
      |    },
      |    {
      |      "_type": "Union",
      |      "child": [
      |        {
      |          "_type": "resource",
      |          "resourceType": "Observation",
      |          "filterSolr": "df1"
      |        },
      |        {
      |          "_type": "resource",
      |          "resourceType": "Encounter",
      |          "filterSolr": "df2"
      |        }
      |      ]
      |    },
      |    {
      |      "_type": "AntiJoin",
      |      "child": [
      |        {
      |          "_type": "resource",
      |          "resourceType": "Patient",
      |          "filterSolr": "df3"
      |        },
      |        {
      |          "_type": "resource",
      |          "resourceType": "Patient",
      |          "filterSolr": "df4"
      |        }
      |      ]
      |    }
      |  ]
      |}
      |""".stripMargin
}
