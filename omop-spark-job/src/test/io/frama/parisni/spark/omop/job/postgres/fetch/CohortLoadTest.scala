/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package scala.io.frama.parisni.spark.omop.job.postgres.fetch

import com.lucidworks.spark.util.SolrSupport
import io.frama.parisni.spark.omop.job.group.OmopTools
import io.frama.parisni.spark.omop.job.{
  PostgresUtils,
  SolrConfTest,
  SolrUtils,
  SparkSessionTestWrapper
}
import org.junit.{Before, Test}

class CohortLoadTest
    extends SolrConfTest
    with SparkSessionTestWrapper
    with PostgresUtils
    with SolrUtils { //FunSuite with io.frama.parisni.spark.sync.copy.SparkSessionTestWrapper

  val cohortJson =
    raw"""
         |{
         |"resourceType": "Composition",
         |""
         |}
         |""".stripMargin
  @Test
  def testCohortLoadTest(): Unit = {

    startSolrCloudCluster
    createSolrCohortTables()

    val options = Map(
      "zkhost" -> zkHost,
      "commit_within" -> "100",
      "batch_size" -> "5000",
      "timezone_id" -> "Europe/Paris",
      "collection" -> "groupAphp",
      "request_handler" -> "/export",
      "fields" -> "id,resourceId,groupId,active,resourceType"
    )

    val cloudSolrClient = SolrSupport.getCachedCloudClient(zkHost)
    cloudSolrClient.setDefaultCollection("source")
    val omopTools = new OmopTools(this.getPgTool(), options)
    // Create table "source"

    import spark.implicits._
    val cohort = Iterator
      .range(0, 1000, 1)
      .toList
      .toDF("subject_id")

    val cohortDefinitionId = omopTools.getCohortDefinitionId(
      "the test cohort name",
      None,
      "{'json':1}",
      1L,
      24
    ) match {
      case Right(a) => a
      case Left(a)  => a
    }
    omopTools.loadCohort(cohortDefinitionId, 1000, cohort)
    val cohort_definition1 =
      getPgTool().sqlExecWithResult("select * from cohort_definition")
    cohort_definition1.show(false)
    assert(
      cohort_definition1
        .filter(
          "cohort_size = 1000 and cohort_status = 'finished' and cohort_type = 'normalized'  and cohort_active = true"
        )
        .count == 1L
    )
    val cohort1 = getPgTool().sqlExecWithResult("select * from cohort")
    assert(cohort1.filter("cohort_definition_id = 1").count == 1000L)
    val group1 = spark.read.format("solr").options(options).load("groupAphp")
    assert(
      group1
        .filter("resourceType = 'Patient' and active = true and groupId = 1")
        .count == 1000L
    )

    val cohortDefinitionId2 = omopTools.getCohortDefinitionId(
      "the test cohort name",
      Some("this is a test cohort long description"),
      "{'json':2}",
      1L,
      24
    )match {
      case Right(a) => a
      case Left(a)  => a
    }
    omopTools.loadCount(cohortDefinitionId2, 1000)
    val cohortDefinition2 =
      getPgTool().sqlExecWithResult("select * from cohort_definition")
    cohortDefinition2.show(false)
    assert(
      cohortDefinition2
        .filter("cohort_definition_id = 2 and cohort_type = 'partial'")
        .count == 1L
    )

    omopTools.loadCohort(cohortDefinitionId2, 1000, cohort)
    val cohortDefinition3 =
      getPgTool().sqlExecWithResult("select * from cohort_definition")
    cohortDefinition3.show(false)
    assert(
      cohortDefinition3
        .filter("cohort_type = 'normalized'")
        .count == 2L
    )
  }

  @Before
  def populateOmopTables(): Unit = {
    getPgTool().sqlExec("""
        |create table cohort_definition (
        | hash integer not null,
        | cohort_definition_id bigserial PRIMARY KEY,
        | cohort_definition_name text,
        | cohort_definition_description text,
        | cohort_definition_syntax text,
        | valid_start_datetime timestamp,
        | owner_entity_id bigint,
        | cohort_initiation_datetime timestamp,
        | cohort_size bigint,
        | cohort_status text,
        | cohort_type text,
        | cohort_hash text,
        | cohort_active boolean
        |)
        |""".stripMargin)

    getPgTool().sqlExec("""
        |create table cohort (
        | hash integer not null,
        | cohort_definition_id bigint not null,
        | cohort_id bigserial PRIMARY KEY,
        | subject_id bigint
        |)
        |""".stripMargin)
  }

}
