SPARK-OMOP
==========

omop-spark-udf
--------------

This module provides several functions, or transformations. In particular it
provides graph transformations that can be used on *fact_relationship* or
*concept_relationship* tables.

omop-solr-sync
--------------

This module load several solr collections from a OMOP dataset. In particular,
it builds indexes for FHIR resources, for Susana and for Cohort360 tools.
Both spark-solr and spark-postgres library are used. In the future, hive and
other connector will be added.

omop-susana-loader
------------------

This module loads informations into a OMOP Susana instance based on postgres.
In particular, it loads the vocabularies into the vocabulary tables.

omop-spark-deid
---------------

This module apply the APHP Deid algorithm on a OMOP dataset backed on hive. In
the future, it will support other jdbc databases such postgres.

omop-spark-standardize
----------------------

This module apply the APHP standardize algorithm on a OMOP dataset. This takes
a local OMOP dataset and produces a standardized result.

omop-spark-quality
------------------

This module apply the APHP quality algorithm on a OMOP dataset. This takes
a local OMOP dataset, a set of rules, and produce an output.

