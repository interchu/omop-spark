appName: OMOP standardization pipeline
inputFolder: test/input
outputFolder: test/output
tmpFolder: test/tmp
optionalSteps:
  qualityInput: true 
  qualityOutput: true
  standardDispatch: true
  standardConcept: true 
  standardDate: true 
  localConcept: true 
  standardUnit: false 
resources:
- name: concept_pivot
  # csv | orc | parquet
  inputFormat: csv
  outputFormat: parquet
  active: true
  csvConfig: # when type = csv. header is mandatory
    delimiter: ','
    quote: '"'
    escapeQuote: '"'
    multiline: False
  schema:
    fields:
    - name: concept_id
      type: integer
      constraints:
        required: true
        unique: true
    - name: concept_code
      type: string
    - name: vocabulary_id
      type: string
    - name: domain_id
      type: string
    - name: standard_concept_id
      type: integer
    - name: standard_domain_id
      type: string
- name: person
  partition: 200 # optional
  # csv | orc | parquet
  inputFormat: csv
  outputFormat: csv
  active: true
  csvConfig: # when type = csv. header is mandatory
    delimiter: ','
    quote: '"'
    escapeQuote: '"'
    multiline: False
  schema:
    fields:
    - name: person_id 
      type: bigint
      constraints:
        required: true
        unique: true
    - name: person_source_value
      type: string
    - name: location_id
      type: bigint
    - name: provider_id
      type: bigint
    - name: care_site_id
      type: bigint
    - name: year_of_birth
      type: bigint
    - name: month_of_birth
      type: bigint
    - name: day_of_birth
      type: bigint
    - name: birth_datetime
      type: datetime
    - name: death_datetime
      type: datetime
    - name: race_concept_id
      type: integer
    - name: race_source_value
      type: string
    - name: race_source_concept_id
      type: integer
    - name: ethnicity_concept_id
      type: integer
    - name: ethnicity_source_value
      type: string
    - name: ethnicity_source_concept_id
      type: integer
    - name: gender_concept_id
      type: integer
    - name: gender_source_value
      type: string
      constraints:
        localVocabularyId: Gender
    - name: gender_source_concept_id  
      type: integer
- name: visit_occurrence
  partition: 200 # optional
  # csv | orc | parquet
  inputFormat: csv
  outputFormat: csv
  active: false
  csvConfig: # when type = csv. header is mandatory
    delimiter: ','
    quote: '"'
    escapeQuote: '"'
    multiline: False
  schema:
    fields:
    - name: visit_occurrence_id
      type: bigint
      constraints:
        unique: true
    - name: visit_occurrence_source_value
      type: text
    - name: person_id
      type: bigint
    - name: preceding_visit_occurrence_id
      type: bigint 
    - name: provider_id
      type: bigint
    - name: care_site_id
      type: bigint
    - name: visit_start_date
      type: date
    - name: visit_start_datetime
      type: timestamp
    - name: visit_end_date
      type: date
    - name: visit_end_datetime
      type: timestamp
    - name: visit_concept_id
      type: integer
    - name: visit_source_value
      type: text
    - name: visit_source_concept_id
      type: int
    - name: visit_type_concept_id
      type: int
    - name: visit_type_source_value
      type: text
    - name: visit_type_source_concept_id
      type: int
    - name: admitted_from_concept_id      
      type: int     
    - name: admitted_from_source_value    
      type: text 
    - name: admitted_from_source_concept_id      
      type: int    
    - name: discharge_to_concept_id
      type: int   
    - name: discharge_to_source_value
      type: text
    - name: discharge_to_source_concept_id
      type: int  
- name: procedure_occurrence
  partition: 200 # optional
  # csv | orc | parquet
  inputFormat: csv
  outputFormat: csv
  active: false
  csvConfig: # when type = csv. header is mandatory
    delimiter: ','
    quote: '"'
    escapeQuote: '"'
    multiline: false
  schema:
    fields:
    - name: procedure_occurrence_id
      type: bigint
      constraints:
        unique: true
    - name: person_id
      type: bigint
    - name: visit_occurrence_id
      type: bigint
    - name: visit_detail_id             
      type: bigint      
    - name: provider_id
      type: bigint
    - name: procedure_date
      type: date
    - name: procedure_datetime
      type: timestamp
    - name: procedure_concept_id
      type: int
    - name: procedure_source_value
      type: text
    - name: procedure_source_concept_id
      type: int
    - name: procedure_type_concept_id
      type: int
    - name: procedure_type_source_value
      type: text
    - name: procedure_type_source_concept_id
      type: int
    - name: modifier_concept_id
      type: int
    - name: modifier_source_value
      type: text
    - name: quantity
      type: bigint
- name: condition_occurrence
  # csv | orc | parquet
  inputFormat: csv
  outputFormat: csv
  active: false
  csvConfig: # when type = csv. header is mandatory
    delimiter: ','
    quote: '"'
    escapeQuote: '"'
    multiline: false
  schema:
    fields:
    - name: condition_occurrence_id
      type: bigint
      constraints:
        unique: true
    - name: person_id
      type: bigint
    - name: visit_occurrence_id
      type: bigint
    - name: visit_detail_id                
      type: bigint
    - name: provider_id
      type: bigint
    - name: condition_start_date
      type: date
    - name: condition_start_datetime
      type: timestamp
    - name: condition_end_date
      type: date
    - name: condition_end_datetime
      type: timestamp       
    - name: condition_concept_id
      type: int
    - name: condition_source_value
      type: text
    - name: condition_source_concept_id
      type: int
    - name: condition_type_concept_id
      type: int
    - name: condition_type_source_value
      type: text
    - name: condition_type_source_concept_id
      type: int
    - name: condition_status_concept_id
      type: int
    - name: condition_status_source_value
      type: text
    - name: condition_status_source_concept_id
      type: int
    - name: stop_reason
      type: text
- name: observation
  # csv | orc | parquet
  inputFormat: csv
  outputFormat: csv
  active: false
  csvConfig: # when type = csv. header is mandatory
    delimiter: ','
    quote: '"'
    escapeQuote: '"'
    multiline: false
  schema:
    fields:
    - name: observation_id
      type: bigint
    - name: person_id
      type: bigint
    - name: visit_occurrence_id
      type: bigint
    - name: visit_detail_id               
      type: bigint      
    - name: provider_id
      type: bigint
    - name: observation_date
      type: date
    - name: observation_datetime
      type: timestamp
    - name: observation_concept_id
      type: int
    - name: observation_source_value
      type: text
    - name: observation_source_concept_id
      type: int
    - name: observation_type_concept_id
      type: int
    - name: observation_type_source_value
      type: text
    - name: observation_type_source_concept_id   
      type: int
    - name: value_as_number
      type: numeric
    - name: value_source_as_number
      type: numeric
    - name: value_as_string
      type: text
    - name: value_as_concept_id
      type: int
    - name: value_source_as_concept_id
      type: int
    - name: value_as_datetime
      type: timestamp
    - name: value_source_value
      type: text
    - name: qualifier_concept_id
      type: int
    - name: qualifier_source_value
      type: text
    - name: unit_concept_id
      type: int
    - name: unit_source_value
      type: text
    - name: unit_source_concept_id
      type: int
    - name: observation_event_id
      type: bigint
    - name: obs_event_field_concept_id
      type: int
- name: location
  # csv | orc | parquet
  inputFormat: csv
  outputFormat: csv
  active: false
  csvConfig: # when type = csv. header is mandatory
    delimiter: ','
    quote: '"'
    escapeQuote: '"'
    multiline: false
  schema:
    fields:
    - name: location_id
      type: bigint
    - name: address_1
      type: text
    - name: address_2
      type: text
    - name: city
      type: text
    - name: state
      type: text
    - name: zip
      type: text
    - name: county
      type: text
    - name: country
      type: text
    - name: location_source_value 
      type: text
    - name: latitude
      type: numeric
    - name: longitude
      type: numeric
dispatch:
- from: procedure_occurrence
  localDomain: procedure
  columnDispatch: procedure_source_concept_id
  to: 
  - name: condition_occurrence
    standardDomain: condition
    dispatchFields:
    - name: condition_occurrence_id
    - name: person_id
      from: person_id
    - name: visit_occurrence_id
      from: visit_occurrence_id
    - name: visit_detail_id                
      from: visit_detail_id
    - name: provider_id
      from: provider_id
    - name: condition_start_date
    - name: condition_start_datetime
      from: procedure_datetime
    - name: condition_end_date
    - name: condition_end_datetime
      from: procedure_datetime
    - name: condition_concept_id
    - name: condition_source_value
      from: procedure_source_value
    - name: condition_source_concept_id
      from: procedure_source_concept_id
    - name: condition_type_concept_id
    - name: condition_type_source_value
    - name: condition_type_source_concept_id
    - name: condition_status_concept_id
    - name: condition_status_source_value
    - name: condition_status_source_concept_id
    - name: stop_reason
- from: condition_occurrence
  localDomain: condition
  columnDispatch: condition_source_concept_id
  to: 
  - name: procedure_occurrence
    standardDomain: procedure
    dispatchFields:
    - name: procedure_occurrence_id
    - name: person_id
      from: person_id
    - name: visit_occurrence_id
      from: visit_occurrence_id
    - name: visit_detail_id             
      from: visit_detail_id
    - name: provider_id
      from: provider_id
    - name: procedure_date
    - name: procedure_datetime
      from: condition_start_datetime
    - name: procedure_concept_id
    - name: procedure_source_value
      from: condition_source_value
    - name: procedure_source_concept_id
      from: condition_source_concept_id
    - name: procedure_type_concept_id
    - name: procedure_type_source_value
    - name: procedure_type_source_concept_id
    - name: modifier_concept_id
    - name: modifier_source_value
    - name: quantity
