# Principle

This pipe is **optional**

OMOP usually provides both **date** and **datetime** columns for algorithms
retro-compatibility.

This pipe produce a `*_date` value given a `*_datetime` field.
