The `writer` pipe writes the OMOP tables to the specified `output folder`. Once the data is written, the pipeline use it as a checkpoint for latter reads.

Several sources are supported:

- [x] csv
- [x] orc
- [x] parquet
- [x] hive
- [ ] postgresql
- [ ] mysql

# Notice

The results are `overwritten`
