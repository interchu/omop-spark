The `reader` pipe reads the OMOP tables from source. Several sources are supported:

- [x] csv
- [x] orc
- [x] parquet
- [x] hive
- [ ] postgresql
- [ ] mysql
