/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
/**
  * This file is part of OMOP-SPARK.
  *
  * OMOP-SPARK is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * OMOP-SPARK is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.interchu.omop

import io.frama.interchu.omop.ConfigYaml._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.types._

object Run extends App {

  val conf = ConfigYaml.getFromPath(filename = args(0))
  val structs = StructTypeGenerator.get(conf)
  val key = generateKey()

  val spark = SparkSession
    .builder()
    .appName(conf.appName)
    .enableHiveSupport()
    .getOrCreate()

  var dfMap = Initializer.init(spark, structs)
  runPipeline(spark, dfMap, conf, structs)

  def runPipeline(
      spark: SparkSession,
      sourcedfMap: Map[String, DataFrame],
      conf: Config,
      structs: Map[String, StructType]
  ): Unit = {

    var dfMap = sourcedfMap
    dfMap = Reader.readAll(spark, conf, dfMap)

    if (conf.optionalSteps.qualityInput)
      dfMap = QualityInput.checkAll(dfMap)

    if (conf.optionalSteps.standardDispatch)
      dfMap = Dispatcher.dispatchAll(spark, conf, dfMap, structs)

    if (conf.optionalSteps.localConcept)
      dfMap = LocalConceptId.conceptIdsAll(dfMap, conf)

    if (conf.optionalSteps.standardConcept)
      dfMap = StandardConceptId.conceptIdsAll(dfMap, conf)

    if (conf.optionalSteps.standardDate)
      dfMap = StandardDate.dateAll(dfMap, conf)

    dfMap = Writer.writeAll(dfMap, conf, structs, key)

    if (conf.optionalSteps.qualityOutput)
      dfMap = QualityOutput.checkAll(dfMap, conf)
  }

  def generateKey(): String = {
    val dfor = java.time.format.DateTimeFormatter.ofPattern("YYYYMMdd-HHmmss")
    val key = java.time.LocalDateTime.now().format(dfor)
    key
  }
}
