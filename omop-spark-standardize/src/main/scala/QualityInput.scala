/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
/**
  *     This file is part of OMOP-SPARK.
  *
  *     OMOP-SPARK is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     OMOP-SPARK is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.interchu.omop
import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._

object QualityInput {

  /**
    *  Validate the schema
    *  and produce a map of dataframes
    *  Also:
    *  - null columns
    *  - cast columns
    */

  def checkAll(dfMap: Map[String, DataFrame]): Map[String, DataFrame] = {
    val result = collection.mutable.Map[String, DataFrame]()
    dfMap.foreach(f => {
      result(f._1) = validate(f._2, f._2.schema)
    })
    result.toMap

    dfMap ++ result.toMap
  }

  def validate(df: DataFrame, schema: StructType): DataFrame = {
    DFTool.castColumns(df, schema)
  }

}
