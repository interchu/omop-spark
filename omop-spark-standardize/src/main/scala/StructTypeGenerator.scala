/**
  *     This file is part of SPARK-OMOP.
  *
  *     SPARK-OMOP is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     SPARK-OMOP is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
/**
  *     This file is part of OMOP-SPARK.
  *
  *     OMOP-SPARK is free software: you can redistribute it and/or modify
  *     it under the terms of the GNU General Public License as published by
  *     the Free Software Foundation, either version 3 of the License, or
  *     (at your option) any later version.
  *
  *     OMOP-SPARK is distributed in the hope that it will be useful,
  *     but WITHOUT ANY WARRANTY; without even the implied warranty of
  *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *     GNU General Public License for more details.
  *
  *     You should have received a copy of the GNU General Public License
  *     along with OMOP-SPARK.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.interchu.omop

import io.frama.interchu.omop.ConfigYaml._
import org.apache.spark.sql.types._
import java.sql.Timestamp

object StructTypeGenerator {

  def get(conf: Config): Map[String, StructType] = {
    val defaultNull = new MetadataBuilder().putNull("default").build

    val result = collection.mutable.Map[String, StructType]()
    for (c <- conf.resources) {
      result(c.name) = StructType(c.schema.fields.map(f => {
        val nullable = !(f.constraints.isDefined && f.constraints.get.required
          .getOrElse(false))
        if (nullable)
          StructField(f.name, transform(f.`type`), true, defaultNull)
        else
          StructField(f.name, transform(f.`type`), false)
      }))
    }
    result.toMap
  }

  def transform(t: String): DataType = {
    t match {
      case "integer"   => IntegerType
      case "int"       => IntegerType
      case "bigint"    => LongType
      case "timestamp" => TimestampType
      case "datetime"  => TimestampType
      case "date"      => DateType
      case "numeric"   => DoubleType
      case "text"      => StringType
      case _           => StringType
    }
  }
}
