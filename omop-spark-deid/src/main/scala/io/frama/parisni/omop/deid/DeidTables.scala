/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.omop.deid

import org.apache.spark.sql.types._

trait DeidTables {

  def TMP_FOLDER = "tmp/"

  def DEID_KNOWLEDGE_TABLE = "deid_knowledge_tmp"

  def DEID_UIMA_TABLE = "deid_note_nlp_uima_tmp"

  def DEID_GPU_TABLE = "deid_note_nlp_gpu_tmp"

  def DEID_NOTE_NLP_TMP = "deid_note_nlp_tmp"

  def DEID_NOTE_NLP_QUALITY_TMP = "deid_note_nlp_quality_tmp"

  def DEID_NOTE_TMP = "deid_note_tmp"

  def DEID_NOTE_SUBSET = "deid_note_subset_tmp"

  val defaultNull = new MetadataBuilder().putNull("default").build

  val schemaObservation = StructType(
    StructField("person_id", LongType, false) ::
      StructField("observation_concept_id", IntegerType, false) ::
      StructField("value_as_string", StringType, false) ::
      StructField("observation_source_value", StringType, false) ::
      StructField("observation_type_concept_id", IntegerType, false) ::
      Nil
  )

  val schemaNote = StructType(
    StructField("note_id", LongType, false) ::
      StructField("note_text", StringType, true) ::
      Nil
  )

  val schemaNoteNlp = StructType(
    StructField("note_id", LongType, false) ::
      StructField("person_id", LongType, false) ::
      StructField("offset_begin", IntegerType, false) ::
      StructField("offset_end", IntegerType, false) ::
      StructField("note_nlp_source_value", StringType, true, defaultNull) ::
      StructField("snippet", StringType, true, defaultNull) ::
      StructField("lexical_variant", StringType, true, defaultNull) ::
      StructField("term_temporal", StringType, true, defaultNull) ::
      StructField("nlp_system", StringType, true, defaultNull) ::
      Nil
  )

}
