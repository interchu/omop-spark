/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.omop.deid

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.SparkSession
import fr.aphp.wind.eds.DeidTools.{
  DeidDateExtractor,
  DeidDateFormatter,
  DeidNameFormatter
}
import io.frama.parisni.spark.dataframe._

/**
  * This produces a <outputNoteNlpDeid> table with
  * name entity and their assigned replacement candidate.
  *
  * In _full_ mode, the table contains every named entity
  * found in the note table and can be considered as the
  * **note_nlp_deid** table. In this mode, the <inputNoteNlpOld>
  * SHALL be provided empty.
  *
  * in _incremental_ mode, the resulting table should be merged
  * with the existing **note_nlp_deid** table with the Merge
  * script provided.
  */
object Assign extends DeidTables with DeidIO with AssignT with App {

  val mode = args(0)
  val database = args(1)
  val sourceDatabase = args(2)
  val inputCandidate = args(3)
  val inputPersonDeid = args(4)
  val inputNoteNlpOld = args(5)
  val outputNoteNlpDeid = args(6)

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Assign")
    .enableHiveSupport()
    .getOrCreate()

  process(
    spark,
    mode,
    database,
    sourceDatabase,
    inputCandidate,
    inputPersonDeid,
    inputNoteNlpOld,
    outputNoteNlpDeid
  )

  /**
    * This step looks for a candidate to replace the named entity.
    * For each patient note, it looks into previous document and assign
    * the same candidate previously chosen.
    *
    * @param spark             String Name of the hive database
    * @param mode              String Name of the hive database
    * @param database          String Name of the hive database
    * @param inputCandidate    String Name of the hive database
    * @param inputPersonDeid   String Name of the hive database
    * @param inputNoteNlpOld   String Name of the hive database
    * @param outputNoteNlpDeid String Name of the hive database
    * @note MISC INPUT: compressed output of GPU (result.csv.bz2)
    * @note TABLE OUTPUT:  ouputNoteNlpDeid
    * @todo
    */
  def process(
      spark: SparkSession,
      mode: String,
      database: String,
      sourceDatabase: String,
      inputCandidate: String,
      inputPersonDeid: String,
      inputNoteNlpOld: String,
      outputNoteNlpDeid: String
  ) = {

    def getCandidate(filter: String) =
      readDataframe(spark, mode, sourceDatabase, inputCandidate)
        .filter(col("type") === lit(filter))
        .select("snippet")

    val dictPrenom = getCandidate("PRENOM")
    val dictNom = getCandidate("NOM")
    val dictMail = getCandidate("MAIL")
    val dictAdresse = getCandidate("ADRESSE")
    val dictVille = getCandidate("VILLE")
    val dictZip = getCandidate("ZIP")
    val dictTel = getCandidate("TEL")
    val dictIpp = getCandidate("IPP")
    val dictNda = getCandidate("NDA")
    val dictSecu = getCandidate("SECU")

    val shift = readDataframe(spark, mode, sourceDatabase, inputPersonDeid)
      .select("person_id", "shift")

    readTempDataframe(spark, DEID_NOTE_NLP_QUALITY_TMP)
      .createOrReplaceTempView("note_nlp_deid_new")

    // the old annotations is in delta format
    readDataframe(spark, "delta", database, inputNoteNlpOld)
      .createOrReplaceTempView("note_nlp_deid_old")

    val noteNlpNew = spark.sql("""
      SELECT 
          note_id        	      	                    
        , person_id     	      	                    
        , offset_begin        	      	                    
        , offset_end          	      	                    
        , note_nlp_source_value            	      	                    
        , snippet         
        , nlp_system      	      	                       	      	                    
        , term_temporal
        , null as lexical_variant      	 
      FROM note_nlp_deid_new
      """)

    val noteNlpOld = spark.sql(f"""
      SELECT
          note_id        	      	                    
        , person_id     	      	                    
        , offset_begin        	      	                    
        , offset_end          	      	                    
        , note_nlp_source_value            	      	                    
        , snippet  
        , nlp_system                  	      	                      	      	                    
        , term_temporal
        , lexical_variant      	 
   	  FROM note_nlp_deid_old
      WHERE person_id IN (
	      SELECT person_id
	      FROM note_nlp_deid_new
	      )
      """)

    val noteNlp = unionPastToPresent(noteNlpNew, noteNlpOld)

    val other = assignOther(
      noteNlp,
      dictNom,
      dictPrenom,
      dictMail,
      dictAdresse,
      dictVille,
      dictZip,
      dictTel,
      dictIpp,
      dictNda,
      dictSecu
    )
    val date = assignDate(noteNlp, shift)
    val header = assignHeader(noteNlp)

    val nothing = assignNothing(
      readTempDataframe(spark, DEID_NOTE_SUBSET),
      readTempDataframe(spark, DEID_NOTE_NLP_QUALITY_TMP)
    )

    val result = DFTool
      .applySchemaSoft(
        other.union(date).union(header).union(nothing),
        schemaNoteNlp
      )
      .withColumn("create_datetime", current_timestamp())

    //writeDataframe(result, mode, database, outputNoteNlpDeid)
    writeTempDataframe(result, DEID_NOTE_NLP_TMP)

  }

  def unionPastToPresent(
      noteNlpNew: DataFrame,
      noteNlpOld: DataFrame
  ): DataFrame = {
    val spark = noteNlpNew.sparkSession
    spark.udf.register("deid_norm_str", deid_norm_str(_: String))
    spark.udf.register(
      "deid_extract_initials",
      deid_extract_initials(_: String)
    )
    spark.udf.register("deid_is_initials", deid_is_initials(_: String))
    spark.udf.register(
      "deid_extract_initials_nice",
      deid_extract_initials_nice(_: String)
    )
    spark.udf.register(
      "deid_name_format",
      deid_name_format(_: String, _: String)
    )
    spark.udf.register("deid_extract_format", deid_extract_format(_: String))
    spark.udf.register(
      "deid_date_format",
      deid_date_format(_: String, _: String)
    )
    spark.udf.register(
      "deid_extract_date_format",
      deid_extract_date_format(_: String)
    )

    DFTool
      .applySchema(noteNlpNew, schemaNoteNlp)
      .withColumn("is_new", lit(1))
      .union(
        DFTool
          .applySchema(noteNlpOld, schemaNoteNlp)
          .withColumn("is_new", lit(0))
      )

  }

  /*
   * OTHERS
   */
  def assignOther(
      noteNlp: DataFrame,
      dictNom: DataFrame,
      dictPrenom: DataFrame,
      dictMail: DataFrame,
      dictAdresse: DataFrame,
      dictVille: DataFrame,
      dictZip: DataFrame,
      dictTel: DataFrame,
      dictIpp: DataFrame,
      dictNda: DataFrame,
      dictSecu: DataFrame
  ): DataFrame = {
    noteNlp.createOrReplaceTempView("note_nlp")
    DFTool
      .dfAddSequence(dictNom, "ids_dico")
      .createOrReplaceTempView("dico_nom")
    DFTool
      .dfAddSequence(dictPrenom, "ids_dico")
      .createOrReplaceTempView("dico_prenom")
    DFTool
      .dfAddSequence(dictMail, "ids_dico")
      .createOrReplaceTempView("dico_mail")
    DFTool
      .dfAddSequence(dictAdresse, "ids_dico")
      .createOrReplaceTempView("dico_adresse")
    DFTool
      .dfAddSequence(dictVille, "ids_dico")
      .createOrReplaceTempView("dico_ville")
    DFTool
      .dfAddSequence(dictZip, "ids_dico")
      .createOrReplaceTempView("dico_zip")
    DFTool
      .dfAddSequence(dictTel, "ids_dico")
      .createOrReplaceTempView("dico_tel")
    DFTool
      .dfAddSequence(dictIpp, "ids_dico")
      .createOrReplaceTempView("dico_ipp")
    DFTool
      .dfAddSequence(dictNda, "ids_dico")
      .createOrReplaceTempView("dico_nda")
    DFTool
      .dfAddSequence(dictSecu, "ids_dico")
      .createOrReplaceTempView("dico_secu")

    noteNlp.sparkSession
      .sql("""
  SELECT 
    note_id
  , person_id
  , offset_begin
  , offset_end
  , note_nlp_source_value
  , snippet
  , lexical_variant
  , nlp_system   
  , term_temporal
  , is_new
  , CASE WHEN note_nlp_source_value = 'PRENOM' 
           THEN deid_extract_initials(snippet)
         ELSE deid_norm_str(snippet)
    END AS snippet_norm
  , CASE 
	    WHEN note_nlp_source_value = 'NOM' 
	      THEN CAST(rand() * (SELECT count(1) FROM dico_nom)  + 1 AS integer)
	    WHEN note_nlp_source_value = 'PRENOM' 
	      THEN CAST(rand() * (SELECT count(1) FROM dico_prenom)  + 1 AS integer)
	    WHEN note_nlp_source_value = 'MAIL' 
	      THEN CAST(rand() * (SELECT count(1) FROM dico_mail) + 1 AS integer)
	    WHEN note_nlp_source_value = 'ADRESSE' 
	      THEN CAST(rand() * (SELECT count(1) FROM dico_adresse) + 1 AS integer)
	    WHEN note_nlp_source_value = 'VILLE' 
	      THEN CAST(rand() * (SELECT count(1) FROM dico_ville) + 1 AS integer)
	    WHEN note_nlp_source_value = 'ZIP' 
	      THEN CAST(rand() * (SELECT count(1) FROM dico_zip) + 1 AS integer)
	    WHEN note_nlp_source_value = 'TEL'
	      THEN CAST(rand() * (SELECT count(1) FROM dico_tel) + 1 AS integer)
	    WHEN note_nlp_source_value = 'IPP'
	      THEN CAST(rand() * (SELECT count(1) FROM dico_ipp) + 1 AS integer)
	    WHEN note_nlp_source_value = 'NDA'
	      THEN CAST(rand() * (SELECT count(1) FROM dico_nda) + 1 AS integer)
	    WHEN note_nlp_source_value = 'SECU'
	      THEN CAST(rand() * (SELECT count(1) FROM dico_secu) + 1 AS integer) 
    END AS random_id
  FROM note_nlp
  WHERE TRUE
	AND note_nlp_source_value 
	IN ( 'NOM', 'PRENOM', 'MAIL', 'ADRESSE', 'VILLE', 'ZIP', 'TEL', 'IPP', 'NDA', 'SECU' )
""").createOrReplaceTempView("gen_random_id")

    noteNlp.sparkSession
      .sql("""
  SELECT 
    note_id
  , person_id
  , offset_begin
  , offset_end
  , note_nlp_source_value
  , gen_random_id.snippet
  , lexical_variant
  , nlp_system   
  , term_temporal
  , is_new
  , snippet_norm
  , random_id
  , CASE 
      WHEN note_nlp_source_value = 'NOM'     THEN dico_nom.snippet
      WHEN note_nlp_source_value = 'PRENOM'  THEN dico_prenom.snippet
      WHEN note_nlp_source_value = 'MAIL'    THEN dico_mail.snippet
      WHEN note_nlp_source_value = 'ADRESSE' THEN dico_adresse.snippet
      WHEN note_nlp_source_value = 'VILLE'   THEN dico_ville.snippet
      WHEN note_nlp_source_value = 'ZIP'     THEN dico_zip.snippet 
      WHEN note_nlp_source_value = 'TEL'     THEN dico_tel.snippet 
      WHEN note_nlp_source_value = 'IPP'     THEN dico_ipp.snippet 
      WHEN note_nlp_source_value = 'NDA'     THEN dico_nda.snippet 
      WHEN note_nlp_source_value = 'SECU'    THEN dico_secu.snippet 
    END AS random_snippet
  FROM gen_random_id
  LEFT JOIN dico_nom      ON (dico_nom.ids_dico = random_id AND note_nlp_source_value = 'NOM')
  LEFT JOIN dico_prenom   ON (dico_prenom.ids_dico = random_id AND note_nlp_source_value = 'PRENOM')
  LEFT JOIN dico_mail     ON (dico_mail.ids_dico = random_id AND note_nlp_source_value = 'MAIL')
  LEFT JOIN dico_adresse  ON (dico_adresse.ids_dico = random_id AND note_nlp_source_value = 'ADRESSE')
  LEFT JOIN dico_ville    ON (dico_ville.ids_dico = random_id AND note_nlp_source_value = 'VILLE')
  LEFT JOIN dico_zip      ON (dico_zip.ids_dico = random_id AND note_nlp_source_value = 'ZIP')
  LEFT JOIN dico_tel      ON (dico_tel.ids_dico = random_id AND note_nlp_source_value = 'TEL')
  LEFT JOIN dico_ipp      ON (dico_ipp.ids_dico = random_id AND note_nlp_source_value = 'IPP')
  LEFT JOIN dico_nda      ON (dico_nda.ids_dico = random_id AND note_nlp_source_value = 'NDA')
  LEFT JOIN dico_secu     ON (dico_secu.ids_dico = random_id AND note_nlp_source_value = 'SECU')
""").createOrReplaceTempView("gen_random_snippet")

    noteNlp.sparkSession
      .sql("""
  SELECT 
   note_id
  , person_id
  , offset_begin
  , offset_end
  , note_nlp_source_value
  , snippet
  , lexical_variant
  , nlp_system   
  , term_temporal
  , is_new
  , snippet_norm
  , random_id
  , random_snippet
  , DENSE_RANK() 
   OVER (PARTITION BY person_id, note_nlp_source_value 
         ORDER BY snippet_norm) 
   AS group_calc
  FROM gen_random_snippet
  """).createOrReplaceTempView("gen_group")

    noteNlp.sparkSession
      .sql("""
  SELECT 
   note_id
  , person_id
  , offset_begin
  , offset_end
  , note_nlp_source_value
  , snippet
  , lexical_variant
  , nlp_system   
  , term_temporal
  , is_new
  , snippet_norm
  , random_id
  , random_snippet
  , group_calc
  , coalesce(
      FIRST(lexical_variant) -- when old value 
	  OVER( PARTITION BY person_id, note_nlp_source_value, group_calc 
	  ORDER BY lexical_variant NULLS LAST) 
	  ,  FIRST(random_snippet) -- first of random values
	  OVER( PARTITION BY person_id, note_nlp_source_value, group_calc 
	  ORDER BY lexical_variant NULLS LAST) ) AS lexical_variant_new
  FROM gen_group
  """).createOrReplaceTempView("gen_variant_new")

    // Affectation des valeurs
    val result = noteNlp.sparkSession.sql("""
  SELECT
    note_id
  , person_id
  , note_nlp_source_value
  , offset_begin
  , offset_end
  , snippet
  , nlp_system   
  , term_temporal
  , CASE 
      WHEN note_nlp_source_value IN ( 'NOM', 'ADRESSE', 'VILLE' )
        THEN deid_name_format(lexical_variant_new, gen_variant_new.snippet)
      WHEN note_nlp_source_value = 'PRENOM' 
        THEN CASE WHEN deid_is_initials(snippet) 
                    THEN deid_extract_initials_nice(lexical_variant_new)
                  ELSE deid_name_format(lexical_variant_new, gen_variant_new.snippet)
             END
      ELSE lexical_variant_new
    END AS lexical_variant
  FROM gen_variant_new
  WHERE is_new = 1
  """)
    DFTool.applySchemaSoft(result, schemaNoteNlp)

  }

  /*
   * DATES
   */
  def assignDate(noteNlp: DataFrame, dictShift: DataFrame): DataFrame = {
    noteNlp.createOrReplaceTempView("note_nlp")
    dictShift.createOrReplaceTempView("shift")
    noteNlp.sparkSession
      .sql("""
  SELECT 
  *
  , deid_extract_date_format(snippet) as date_formating
  FROM note_nlp
  WHERE note_nlp_source_value = 'DATE'
    """)
      .createOrReplaceTempView("deidDateTyped")

    //Shift
    noteNlp.sparkSession
      .sql("""
  SELECT 
  deidDateTyped.*
  , CASE 
      WHEN term_temporal RLIKE '^concept_date=[0-9]{4}-[0-9]{2}-[0-9]{2}$' 
        THEN DATE_ADD(TO_DATE(CAST(UNIX_TIMESTAMP(substring(term_temporal, 14), 'yyyy-MM-dd') AS TIMESTAMP)), shift)
      WHEN term_temporal RLIKE '^concept_date=[0-9]{4}-[0-9]{2}$' 
        THEN DATE_ADD(TO_DATE(CAST(UNIX_TIMESTAMP(substring(term_temporal, 14), 'yyyy-MM') AS TIMESTAMP)), shift)
      WHEN term_temporal RLIKE '^concept_date=[0-9]{4}$' 
        THEN DATE_ADD(TO_DATE(CAST(UNIX_TIMESTAMP(substring(term_temporal, 14), 'yyyy') AS TIMESTAMP)), shift)
    END as date_shifted
  FROM deidDateTyped
  LEFT JOIN shift ON (shift.person_id = deidDateTyped.person_id)
""").createOrReplaceTempView("deidDateShifted")

    // formattage
    val result = noteNlp.sparkSession.sql("""
  SELECT 
    note_id
  , person_id
  , note_nlp_source_value
  , offset_begin
  , offset_end
  , snippet
          , nlp_system   
  , term_temporal
  , CASE 
      WHEN term_temporal IS NOT NULL AND date_shifted IS NOT NULL 
        THEN deid_date_format( date_formating, date_shifted ) 
      ELSE '<????-??-??>' 
    END as lexical_variant
  FROM deidDateShifted
  WHERE is_new = 1
""")
    DFTool.applySchemaSoft(result, schemaNoteNlp)

  }

  /*
   * HEADERS
   */
  def assignHeader(noteNlp: DataFrame): DataFrame = {
    noteNlp.createOrReplaceTempView("note_nlp")
    val result = noteNlp.sparkSession.sql("""
  SELECT 
    note_id
  , person_id
  , note_nlp_source_value
  , offset_begin
  , offset_end
  , snippet
          , nlp_system   
  , null as term_temporal
  , '' as lexical_variant
  FROM note_nlp
  WHERE note_nlp_source_value in( 'HEADER', 'FOOTER' )
  AND is_new = 1
    """)
    DFTool.applySchemaSoft(result, schemaNoteNlp)

  }

  def assignNothing(inputNote: DataFrame, candidate: DataFrame): DataFrame = {
    val result = inputNote
      .as("s")
      .join(
        candidate,
        Seq("note_id"),
        "leftanti"
      ) //those who have noting to hide
      .selectExpr(
        "s.note_id",
        "s.person_id",
        "'NOTHING' note_nlp_source_value",
        "0 as offset_begin",
        "0 as offset_end",
        "'' as snippet",
        "null as term_temporal",
        "'' as lexical_variant"
      )
    DFTool.applySchemaSoft(result, schemaNoteNlp)

  }

}

trait AssignT {

  def deid_norm_str(value: String) = {
    if (value == null) {
      null
    } else {
      value.toLowerCase.replaceAll("\\p{Punct}|\\s", "")
    }
  }

  def deid_extract_initials(value: String) = {
    if (value == null) {
      null
    } else {
      value
        .replaceAll("([^\\s|^\\p{Punct}])[^\\s|\\p{Punct}]+", "$1")
        .replaceAll("\\s|\\p{Punct}", "")
        .toLowerCase
    }
  }

  def deid_is_initials(value: String) = {
    if (value == null) {
      false
    } else {
      value.matches("(\\p{L}\\p{Punct}?)|(\\p{L}(\\p{Punct}\\p{L}?)+)+")
    }
  }

  def deid_extract_initials_nice(value: String) = {
    if (value == null) {
      null
    } else {
      value
        .replaceAll("([^\\s|^\\p{Punct}])[^\\s|\\p{Punct}]+", "$1")
        .replaceAll("\\s|\\p{Punct}|$", ".")
    }
  }

  def deid_name_format(value: String, original: String) = {
    DeidNameFormatter.format(value, original)
  }

  def deid_extract_format(value: String) = {
    if (value == null) {
      null
    } else {
      value
        .replaceAll("([^\\s|^\\p{Punct}])[^\\s|\\p{Punct}]+", "$1")
        .replaceAll("\\s|\\p{Punct}|$", ".")
    }
  }

  def deid_date_format(format: String, value: String) = {
    DeidDateFormatter.format(format, value, "fr")
  }

  def deid_extract_date_format(value: String) = {
    if (value == null) {
      null
    } else {
      DeidDateExtractor.extract(value)
    }
  }

}
