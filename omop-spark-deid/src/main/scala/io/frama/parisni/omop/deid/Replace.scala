/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.omop.deid

import com.typesafe.scalalogging.LazyLogging
import fr.aphp.wind.eds.DeidTools.DeidTools
import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
  * This produce a <outputNoteDeid> table with deidentified
  * note in it, given an <inputNote> table and also a
  * <inputNoteNlpDeid> table previously built from the pipeline.
  *
  * The <phiString> lists which kind of entity will be removed
  * and replaced. It is yet possible to only replace NOM and PRENOM.
  */
object Replace extends DeidTables with DeidIO with App with LazyLogging {

  case class NoteToReplace(
      note_id: Long,
      note_text: String,
      offset_begin: Array[Integer],
      offset_end: Array[Integer],
      lexical_variant: Array[String]
  )

  val mode = args(0)
  val database = args(1)
  val phiString = args(2)
  val inputNote = args(3)
  val inputNoteNlpDeid = args(4)
  val outputNoteDeid = args(5)
  val deidmode = args(6)

  require(args.size == 7, "Provide 7 arguments")

  val spark = SparkSession
    .builder()
    .appName("OMOP DEID Replace")
    .enableHiveSupport()
    .getOrCreate()

  process(
    spark,
    mode,
    database,
    phiString,
    inputNote,
    inputNoteNlpDeid,
    outputNoteDeid,
    deidmode
  )

  /**
    * This step use the note_nlp results and replace the
    * informations within the texts
    *
    * @param mode             String Either hive or csv
    * @param database         String Name of the hive database or folder path
    * @param phiString        String Comma separated list of phi to replace
    * @param inputNote        String The input note table name
    * @param inputNoteNlpDeid String The input note nlp deid table name
    * @param outputNoteDeid   String The output note deid table name
    * @note TABLE OUTPUT:  [[DEID_NOTE_TMP]]
    */
  def process(
      spark: SparkSession,
      mode: String,
      database: String,
      phiString: String,
      inputNote: String,
      inputNoteNlpDeid: String,
      outputNoteDeid: String,
      deidmode: String
  ) = {

    require(
      outputNoteDeid != inputNote,
      "input and output note table cannot be the same"
    )

    //val note = readTempDataframe(spark, DEID_NOTE_SUBSET)
    val note = readDataframe(spark, mode, database, inputNote)

    // the note deid table is stored as delta
    val noteNlpDeid = readDataframe(spark, "delta", database, inputNoteNlpDeid)

    val result = replace(spark, noteNlpDeid, note, deidmode, phiString)
      .withColumn("create_datetime", current_timestamp())

    writeDataframe(result, mode, database, outputNoteDeid)

  }

  def extractPhiList =
    (x: String) => x.split(",").map(x => x.trim.toLowerCase()).toList

  def replace(
      spark: SparkSession,
      noteDeidTmp: DataFrame,
      note: DataFrame,
      mode: String = "candidate",
      phiString: String = "*"
  ): DataFrame = {
    require(List("candidate", "pseudo").contains(mode))

    val replacement = mode match {
      case "candidate" => "coalesce(lexical_variant,'<????>')"
      case "pseudo" =>
        "coalesce(concat('[', note_nlp_source_value ,']'),'[unknown]')"
    }
    if (phiString == "*") {
      noteDeidTmp
      //    .repartition(1000, col("note_id"))
        .createOrReplaceTempView("note_deid_tmp")
    } else {
      val phiList = extractPhiList(phiString)
      logger.info("Replacing : %s".format(phiList))
      noteDeidTmp
        .filter(lower(col("note_nlp_source_value")).isin(phiList: _*))
        //   .repartition(1000, col("note_id"))
        .createOrReplaceTempView("note_deid_tmp")
    }

    import spark.implicits._
    note
    // .repartition(1000, col("note_id"))
      .createOrReplaceTempView("note")

    spark
      .sql(s"""
    select 
        note_id
      , collect_list(offset_begin) as offset_begin
      , collect_list(offset_end) as offset_end
      , collect_list(lexical_variant) as lexical_variant 
    from (
       select note_id
	        , offset_begin
	        , offset_end
	        , $replacement as lexical_variant
       from  note_deid_tmp
       order by note_id, offset_begin ASC, offset_end ASC
       ) as deid 
    group by note_id
    """)
      .createOrReplaceTempView("arrays")

    val result = spark
      .sql("""
    select 
        note.note_id
      , note.note_text
      , arrays.offset_begin
      , arrays.offset_end
      , arrays.lexical_variant 
    FROM note 
    LEFT JOIN arrays 
      ON (arrays.note_id = note.note_id)
      """)
      .as[NoteToReplace]

    val docResult = result.map(row =>
      (
        row.note_id,
        DeidTools.replaceOffsets(
          row.note_text,
          row.offset_begin,
          row.offset_end,
          row.lexical_variant
        )
      )
    )

    DFTool.applySchemaSoft(docResult.toDF("note_id", "note_text"), schemaNote)

  }
}
