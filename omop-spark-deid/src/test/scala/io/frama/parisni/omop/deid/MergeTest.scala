/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */

package io.frama.parisni.omop.deid

import org.apache.spark.sql.{DataFrame, QueryTest, SaveMode}

class MergeTest extends QueryTest with SparkSessionTestWrapper {

  test("test knowledge") {

    import spark.implicits._

    val source = List(
      (1L, 1L, 1, 6, 1234)
    ).toDF("note_nlp_id", "note_id", "offset_begin", "offset_end", "note_hash")
    source.write
      .format("delta")
      .option("mergeSchema", true)
      .mode(SaveMode.Overwrite)
      .save("/tmp/test")

    val candidate = List(
      (1L, 1L, 1, 6, 1234),
      (1L, 2L, 7, 16, 1234),
      (2L, 2L, 7, 16, 1234)
    ).toDF("note_nlp_id", "note_id", "offset_begin", "offset_end", "note_hash")
    Merge.deltaMerge(candidate, "/tmp/", "test", "note_nlp_id,offset_begin")

    checkAnswer(getDeltaTable("/tmp/test"), candidate)

    val candidate2 = List(
      (1L, 1L, 1, 6, 12346),
      (1L, 2L, 7, 16, 12346),
      (2L, 2L, 7, 16, 1234)
    ).toDF("note_nlp_id", "note_id", "offset_begin", "offset_end", "note_hash")
    Merge.deltaMerge(candidate2, "/tmp/", "test", "note_nlp_id,offset_begin")

    checkAnswer(getDeltaTable("/tmp/test"), candidate2)
  }

  def getDeltaTable(path: String): DataFrame = {
    spark.read.format("delta").load(path)
  }
}
