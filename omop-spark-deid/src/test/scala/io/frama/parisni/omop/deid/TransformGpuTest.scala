/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.omop.deid

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.types._

class TransformGpuTest
    extends QueryTest
    with SparkSessionTestWrapper
    with DeidTables {

  test("test prenom") {

    val gpuJson =
      "[{'begin':13,'end':20,'type':'PRENOM'},{'begin':21,'end':26,'type':'NOM'}]"
        .replace("'", "\"")
    val df = spark
      .sql(f"""
      select 1 as note_id, '$gpuJson' as json
      """)
      .cache

    val dfGold = spark
      .sql("""
      select 
        1 as note_id
      , 13 as offset_begin
      , 20 as offset_end
      , 'PRENOM' as note_nlp_source_value
      UNION ALL
      select 1 as note_id
      , 21 as offset_begin
      , 26 as offset_end
      , 'NOM' as note_nlp_source_value
      """)
      .cache
    val schema = StructType(
      StructField("note_id", LongType, false) ::
        StructField("offset_begin", LongType, false) ::
        StructField("offset_end", LongType, false) ::
        Nil
    )
    val dfResult = TransformGpu.jsonExplode(df)
    //    dfGold.show()
    //    dfResult.show
    checkAnswer(
      DFTool.applySchema(dfGold, schema),
      DFTool.applySchema(dfResult, schema)
    )
  }

  test("test snippet") {
    val textValue = "Je m''appelle Nicolas Paris"
    val gpuJson =
      "[{'begin':12,'end':18,'type':'PRENOM'},{'begin':20,'end':24,'type':'NOM'}]"
        .replace("'", "\"")

    val df = spark
      .sql(f"""
      select 1 as note_id, '$gpuJson' as json
      """)
      .cache

    val text = spark
      .sql(f"""
      select 1 as note_id, 2 as person_id, '$textValue' as note_text
      """)
      .cache

    val dfGold = spark
      .sql("""
      select 
        1 as note_id
    	, 2 as person_id
      , 12 as offset_begin
      , 18 as offset_end
      , 'Nicolas' as snippet
      , 'PRENOM' as note_nlp_source_value
      , 'neuroner' nlp_system
      , cast(null as string) term_temporal
      UNION ALL
      select 1 as note_id
      , 2 as person_id
      , 20 as offset_begin
      , 24 as offset_end
      , 'Paris' as snippet
      , 'NOM' as note_nlp_source_value
      , 'neuroner' nlp_system
      , cast(null as string) term_temporal
      """)
      .cache

    val dfGold2 = DFTool.applySchema(dfGold, schemaNoteNlp)
    val dfExplodedResult = TransformGpu.jsonExplode(df)
    val dfResult = TransformGpu.getSnippet(dfExplodedResult, text)
    //    dfGold2.show()
    //    dfResult.show()
    checkAnswer(dfGold2.orderBy("offset_begin"), dfResult.orderBy("offset_end"))
  }

}
