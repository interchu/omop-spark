
CREATE OR REPLACE FUNCTION omop.dates()
  RETURNS trigger AS
$BODY$
BEGIN
    IF TG_OP = 'INSERT' THEN
        NEW.insert_datetime := now();
        NEW.update_datetime := NULL;
        NEW.change_datetime := now();
    ELSIF TG_OP = 'UPDATE' THEN
        NEW.insert_datetime := OLD.insert_datetime;
        NEW.update_datetime := now();
        NEW.change_datetime := now();
    END IF;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE TRIGGER person_dates
  BEFORE INSERT OR UPDATE
  ON omop.person
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER visit_occurrence_dates
  BEFORE INSERT OR UPDATE
  ON omop.visit_occurrence
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER visit_detail_dates
  BEFORE INSERT OR UPDATE
  ON omop.visit_detail
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER condition_occurrence_dates
  BEFORE INSERT OR UPDATE
  ON omop.condition_occurrence
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER procedure_occurrence_dates
  BEFORE INSERT OR UPDATE
  ON omop.procedure_occurrence
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER drug_exposure_dates
  BEFORE INSERT OR UPDATE
  ON omop.drug_exposure
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER measurement_physio_dates
  BEFORE INSERT OR UPDATE
  ON omop.measurement_physio
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER measurement_labs_dates
  BEFORE INSERT OR UPDATE
  ON omop.measurement_labs
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER note_content_dates
  BEFORE INSERT OR UPDATE
  ON omop.note_content
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER note_section_dates
  BEFORE INSERT OR UPDATE
  ON omop.note_section
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_relationship_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept_relationship
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_synonym_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept_synonym
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER vocabulary_dates
  BEFORE INSERT OR UPDATE
  ON omop.vocabulary
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER cost_dates
  BEFORE INSERT OR UPDATE
  ON omop.cost
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER concept_fhir_dates
  BEFORE INSERT OR UPDATE
  ON omop.concept_fhir
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER cohort_definition_dates
  BEFORE INSERT OR UPDATE
  ON omop.cohort_definition
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER cohort_dates
  BEFORE INSERT OR UPDATE
  ON omop.cohort
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER provider_dates
  BEFORE INSERT OR UPDATE
  ON omop.provider
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER observation_dates
  BEFORE INSERT OR UPDATE
  ON omop.observation
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER care_site_dates
  BEFORE INSERT OR UPDATE
  ON omop.care_site
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER care_site_history_dates
  BEFORE INSERT OR UPDATE
  ON omop.care_site_history
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER location_dates
  BEFORE INSERT OR UPDATE
  ON omop.location
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER location_history_dates
  BEFORE INSERT OR UPDATE
  ON omop.location_history
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER fact_relationship_dates
  BEFORE INSERT OR UPDATE
  ON omop.fact_relationship
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER role_dates
  BEFORE INSERT OR UPDATE
  ON omop.role
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER job
  BEFORE INSERT OR UPDATE
  ON omop.job
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER job_definition
  BEFORE INSERT OR UPDATE
  ON omop.job_definition
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();

CREATE TRIGGER relationship
  BEFORE INSERT OR UPDATE

  ON omop.relationship
  FOR EACH ROW
  EXECUTE PROCEDURE omop.dates();
