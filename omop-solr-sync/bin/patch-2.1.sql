-- add fields for cohort
set search_path to omop;
alter table cohort_definition add column cohort_active boolean;
alter table cohort_definition add column cohort_status text;
alter table cohort_definition add column cohort_type text; 

-- remove unused fields
alter table concept drop column m_language_id;
alter table concept drop column m_project_id;

update cohort_definition set cohort_active = true, cohort_status='finished', cohort_type='denormalized';
insert into cohort_definition (
hash                            
,cohort_definition_id      
,cohort_definition_source_value  
,cohort_definition_name          
,definition_type_concept_id      
,subject_concept_id              
,subject_source_concept_id       
,cohort_initiation_datetime      
,owner_entity_id                 
,owner_domain_id                 
,cohort_size                     
,valid_start_datetime            
,row_id                          
,cohort_active                   
,cohort_status                   
,cohort_type                     
)
values
(-1, 40000, 'cohort_360_40000', 'random Cohort Set', 0, 56, 1, now(), 676, 'Provider', 40000, now(), true, 'finished', 'normalized');
