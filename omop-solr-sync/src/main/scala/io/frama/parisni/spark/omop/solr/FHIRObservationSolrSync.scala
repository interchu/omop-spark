/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.solr

import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

class FHIRObservationSolrSync(
    spark: SparkSession,
    options: Map[String, String],
    url: String,
    deltaPath: String
) extends SolrSync(spark, options, url, deltaPath)
    with T {

  _collection = "observationAphp"

  override def transformDelta(conceptFhir: DataFrame): this.type = {

    logger.warn("Generating %s from OMOP".format(_collection))
    var resource = spark.read.format("delta").load(deltaPath + "measurement")
    this.dfTransformed = FHIRObservationSolrSync.transform(
      resource.repartition(_partitionNumLarge),
      conceptFhir
    )
    this.dfTransformed = DFTool.applySchemaSoft(
      this.dfTransformed,
      FHIRObservationSolrSync.outputSchema
    )

    writeCollection(joinStr = "encounterAphp")

    this

  }

}

object FHIRObservationSolrSync extends T {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, true)
      :: StructField("patient", LongType, false)
      :: StructField("patient-type", StringType, false)
      :: StructField("encounter", LongType, true)
      :: StructField("encounter-type", StringType, true)
      :: StructField("category", ArrayType(StringType), false)
      :: StructField("category-simple", StringType, false)
      :: StructField("code", ArrayType(StringType), false)
      :: StructField("code-simple", StringType, false)
      :: StructField("status", ArrayType(StringType), true)
      :: StructField("status-simple", StringType, true)
      :: StructField("_text_", StringType, true)
      :: StructField("valueQuantity.value", DoubleType, true)
      :: StructField("valueQuantity.unit", StringType, true)
      :: StructField("valueQuantity.system", StringType, true)
      :: StructField("valueQuantity.code", StringType, true)
      :: StructField("valueCodeableConcept", ArrayType(StringType), true)
      :: StructField("valueString", StringType, true)
      :: StructField("interpretation", ArrayType(StringType), true)
      :: Nil
  )

  def apply(
      spark: SparkSession,
      options: Map[String, String],
      url: String,
      deltaPath: String
  ): FHIRObservationSolrSync = {
    val a = new FHIRObservationSolrSync(spark, options, url, deltaPath)
    return a
  }

  def transform(facts: DataFrame, concepts: DataFrame): DataFrame = {

    val spark = facts.sparkSession
    import spark.implicits._
    val resultDf = DFTool.applySchemaSoft(facts, observationSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(concepts, conceptFhirSchema)

    val interpretation: Column = {
      val rlow = $"f.range_low"
      val rhigh = $"f.range_high"
      val value = $"f.value_as_number"
      val system =
        "http://terminology.hl7.org/CodeSystem/v3-ObservationInterpretation"
      val normal = Array(s"${system}|N")
      val low = Array(s"${system}|L")
      val high = Array(s"${system}|H")

      when(
        rlow.isNotNull && rhigh.isNotNull,
        when(value.between(rlow, rhigh), normal)
          .when(value.>(rhigh), high)
          .otherwise(low)
      )
        .when(
          rlow.isNull && rhigh.isNotNull,
          when(value.<=(rhigh), normal).otherwise(high)
        )
        .when(
          rlow.isNotNull && rhigh.isNull,
          when(value.>=(rlow), normal).otherwise(low)
        )
        .otherwise(lit(null))
    }

    val ret = getLastUpdatedDf(
      "f" :: "c" :: "cs" :: "u" :: "cv" :: Nil,
      resultDf
        .alias("f")
        .join(
          conceptFhirDf.alias("code"),
          $"f.measurement_source_concept_id" === $"code.concept_id",
          "left"
        )
        .join(
          conceptFhirDf.alias("cs"),
          $"f.row_status_source_concept_id" === $"cs.concept_id",
          "left"
        )
        .join(
          conceptFhirDf.alias("c"),
          $"f.operator_source_concept_id" === $"c.concept_id",
          "left"
        )
        .join(
          conceptFhirDf.alias("u"),
          $"f.unit_source_concept_id" === $"u.concept_id",
          "left"
        )
        .join(
          conceptFhirDf.alias("cv"),
          $"f.value_source_as_concept_id" === $"cv.concept_id",
          "left"
        )
        .join(
          (conceptFhirDf).alias("v"),
          col("f.measurement_type_source_concept_id") === col("v.concept_id"),
          "left"
        )
        .withColumnRenamed("measurement_id", "id")
        .withColumnRenamed("measurement_datetime", "effectiveDatetime")
        .withColumn("patient", $"person_id")
        .withColumn("patient-type", lit("Patient"))
        .withColumn("encounter", $"visit_occurrence_id")
        .withColumn("encounter-type", lit("Encounter"))
        .withColumn("_text_", fhirNarrative("c", "cs", "u", "cv"))
        .withColumn("code", fhirTokenArray("code"))
        .withColumn("code-simple", fhirTokenSimple("code"))
        .withColumn("category", fhirTokenArray("v"))
        .withColumn("category-simple", fhirTokenSimple("v"))
        .withColumn("status", fhirTokenArray("cs"))
        .withColumn("status-simple", fhirTokenSimple("cs"))
        .withColumn("valueQuantity.value", $"f.value_as_number")
        .withColumn("valueQuantity.unit", $"u.concept_name")
        .withColumn("valueQuantity.code", $"u.concept_code")
        .withColumn("valueQuantity.system", $"u.vocabulary_reference")
        .withColumn("valueCodeableConcept", fhirTokenArray("cs"))
        .withColumn(
          "valueString",
          when(
            $"f.value_as_number".isNull && $"f.value_source_as_concept_id".isNull,
            $"value_source_value"
          )
        )
        .withColumn("referenceRange.low", $"range_low")
        .withColumn("referenceRange.high", $"range_high")
        .withColumn("interpretation", interpretation)
    )
      .selectExpr(
        "_lastUpdated",
        "id",
        "patient",
        "`patient-type`",
        "encounter",
        "`encounter-type`",
        "status",
        "`status-simple`",
        "_text_",
        "code",
        "`code-simple`",
        "category",
        "`category-simple`",
        "effectiveDatetime",
        "`valueQuantity.value`",
        "`valueQuantity.unit`",
        "`valueQuantity.code`",
        "`valueQuantity.system`",
        "valueCodeableConcept",
        "valueString",
        "`referenceRange.low`",
        "`referenceRange.high`",
        "interpretation"
      )

    return DFTool.applySchemaSoft(ret, outputSchema)
  }

}
