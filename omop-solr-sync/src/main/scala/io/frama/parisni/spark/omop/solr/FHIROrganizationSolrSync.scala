/**
  * This file is part of SPARK-OMOP.
  *
  * SPARK-OMOP is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * SPARK-OMOP is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with SPARK-OMOP.  If not, see <https://www.gnu.org/licenses/>.
  */
package io.frama.parisni.spark.omop.solr

import com.typesafe.scalalogging.LazyLogging
import io.frama.parisni.spark.dataframe.DFTool
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import io.frama.parisni.spark.omop.graph.HeritageGraph

class FHIROrganizationSolrSync(
    spark: SparkSession,
    options: Map[String, String],
    url: String,
    deltaPath: String
) extends SolrSync(spark, options, url, deltaPath)
    with T {

  _collection = "organizationAphp"

  override def transformDelta(concept_fhir: DataFrame): this.type = {

    logger.warn("Generating %s from OMOP".format(_collection))
    val care_site = spark.read.format("delta").load(deltaPath + "care_site")
    val fact_relationship = spark.read
      .format("delta")
      .load(deltaPath + "fact_relationship")
      .filter(
        col("domain_concept_id_1") === lit(57)
          && col("domain_concept_id_2") === lit(57)
      )
      .dropDuplicates("fact_id_1", "fact_id_2", "relationship_concept_id")
    this.dfTransformed = FHIROrganizationSolrSync.transform(
      care_site,
      fact_relationship,
      concept_fhir
    )
    this.dfTransformed = DFTool.applySchemaSoft(
      this.dfTransformed,
      FHIROrganizationSolrSync.outputSchema
    )

    writeCollection()

    this
  }
}

object FHIROrganizationSolrSync extends T with LazyLogging {

  val outputSchema = StructType(
    StructField("id", StringType, false)
      :: StructField("_lastUpdated", TimestampType, false)
      :: StructField("identifier", ArrayType(StringType), true)
      :: StructField("identifier-simple", StringType, true)
      :: StructField("type", ArrayType(StringType), true)
      :: StructField("type-simple", StringType, true)
      :: StructField("name", ArrayType(StringType), true)
      :: StructField("name-simple", StringType, true)
      :: StructField("partof", LongType, true)
      :: StructField("partof-type", StringType, true)
      :: StructField("active", BooleanType, true)
      :: StructField("above", ArrayType(LongType), true)
      :: StructField("below", ArrayType(LongType), true)
      :: StructField("_text_", StringType, true)
      :: Nil
  )

  def apply(
      spark: SparkSession,
      options: Map[String, String],
      url: String,
      deltaPath: String
  ): FHIROrganizationSolrSync = {
    val a = new FHIROrganizationSolrSync(spark, options, url, deltaPath)
    return a
  }

  //def getArrayFromGraph(vertice: DataFrame, edge: DataFrame) = {

  //  val hg = new HeritageGraph
  //  hg.calcTopLevelHierarcy(vertice, edge)

  //}

  def transform(
      fact: Dataset[Row],
      factRelationship: Dataset[Row],
      concept: Dataset[Row]
  ): Dataset[Row] = {

    val careSiteDf = DFTool.applySchemaSoft(fact, careSiteSchema)
    val factRelationshipDf =
      DFTool.applySchemaSoft(factRelationship, factRelationshipSchema)
    val conceptFhirDf = DFTool.applySchemaSoft(concept, conceptFhirSchema)

    val hg = new HeritageGraph

    val aboveDF = hg
      .calcTopLevelHierarcy(
        careSiteDf.select("care_site_id", "care_site_name").repartition(2),
        factRelationshipDf
          .filter(col("relationship_concept_id") === lit(46233689))
          .select("fact_id_1", "fact_id_2")
          .repartition(4),
        30
      ) // arbitrary deapth in case of cyclic hiearchy
      .withColumnRenamed("path", "above")
      .withColumnRenamed("id", "hier_id")

    val belowDF = hg
      .calcTopLevelHierarcy(
        careSiteDf.select("care_site_id", "care_site_name").repartition(2),
        factRelationshipDf
          .filter(col("relationship_concept_id") === lit(46233688))
          .select("fact_id_1", "fact_id_2")
          .repartition(4),
        30
      )
      .withColumnRenamed("path", "below")
      .withColumnRenamed("id", "hier_id")

    val ret = getLastUpdatedDf(
      "f" :: "rel" :: "v" :: Nil,
      careSiteDf
        .alias("f")
        .join(
          factRelationshipDf.as("rel"),
          col("f.care_site_id") === col("rel.fact_id_1") && col(
            "rel.relationship_concept_id"
          ) === lit(46233688),
          "left"
        )
        .join(
          conceptFhirDf.alias("v"),
          col("place_of_service_source_concept_id") === col("v.concept_id"),
          "left"
        )
        .join(
          aboveDF.as("af"),
          col("af.hier_id") === col("f.care_site_id"),
          "left"
        )
        .join(
          belowDF.as("bf"),
          col("bf.hier_id") === col("f.care_site_id"),
          "left"
        )
        .withColumn("type", fhirTokenArray("v"))
        .withColumn(
          "identifier",
          array(
            concat(
              lit(
                "https://fhir.eds.aphp.fr/ConceptCode/organization-identifier"
              ),
              lit("|"),
              lit("aphp-code"),
              lit("|"),
              col("care_site_source_value")
            )
          )
        )
        .withColumn("identifier-simple", col("care_site_source_value"))
        .withColumn("partof", col("fact_id_2"))
        .withColumn("partof-type", lit("Organization"))
        .withColumn(
          "name",
          array(col("care_site_name"), col("care_site_short_name"))
        )
        .withColumn("name-simple", col("care_site_short_name"))
        .withColumn("type-simple", fhirTokenSimple("v"))
        .withColumn(
          "_text_",
          fhirNarrativeCol(
            col("care_site_source_value"),
            col("care_site_name"),
            col("care_site_short_name"),
            fhirNarrative("v")
          )
        )
        .withColumn("active", col("valid_end_date").isNull)
    )
      .withColumnRenamed("care_site_id", "id")
      .select(
        "id",
        "name",
        "name-simple",
        "_lastUpdated",
        "identifier",
        "identifier-simple",
        "type",
        "type-simple",
        "partof",
        "partof-type",
        "active",
        "above",
        "below",
        "_text_"
      )

    return DFTool.applySchemaSoft(ret, outputSchema)

  }
}
